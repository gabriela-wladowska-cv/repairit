﻿namespace AlgorithmsComparer
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using BenchmarkDotNet.Attributes;
    using BenchmarkDotNet.Configs;
    using BenchmarkDotNet.Exporters;
    using BenchmarkDotNet.Exporters.Csv;
    using BenchmarkDotNet.Order;

    [MemoryDiagnoser]
    [Orderer(SummaryOrderPolicy.FastestToSlowest)]
    [RankColumn]
    [Config(typeof(Config))]
    [RPlotExporter]
    public class Tests
    {
        [ParamsSource(nameof(ValuesForPassword))]
        public string Password { get; set; }

        public IEnumerable<string> ValuesForPassword => new[]
                                                        {
                                                            "mt1z5s",
                                                            "680tpg5kmf",
                                                            "!2R!+e",
                                                            "J1iI0-s5=g"
                                                        };

        public class Config : ManualConfig
        {
            public Config()
            {
                AddExporter(CsvMeasurementsExporter.Default);
                AddExporter(RPlotExporter.Default);
            }
        }

        [Benchmark]
        public byte[] EncryptPasswordWithSHA256()
        {
            var passwordInBytes = Encoding.UTF8.GetBytes(Password);
            byte[] result;

            using (var sha256 = SHA256.Create())
            {
                result = sha256.ComputeHash(passwordInBytes);
            }

            return result;
        }

        [Benchmark]
        public byte[] EncryptPasswordWithAES()
        {
            byte[] result;

            using (var aes = Aes.Create())
            {
                var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(Password);
                        }
                        result = msEncrypt.ToArray();
                    }
                }
            }

            return result;
        }

        [Benchmark]
        public byte[] EncryptPasswordWithPBKDF2()
        {
            var provider = new RNGCryptoServiceProvider();
            var salt = new byte[24];

            provider.GetBytes(salt);

            var pbkdf2 = new Rfc2898DeriveBytes(Password, salt, 1000);

            return pbkdf2.GetBytes(24);
        }
    }
}
