﻿using System;

namespace AlgorithmsComparer
{
    using BenchmarkDotNet.Running;

    class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<Tests>();
        }
    }
}
