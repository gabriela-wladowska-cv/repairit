﻿<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@500&display=swap" rel="stylesheet">
    <title>Błąd</title>

    <link rel="stylesheet" href="/Content/bootstrap.css">
    <link rel="stylesheet" href="/Content/Site.css">

    <script src="/Scripts/jquery-3.6.0.js"></script>
    <script src="/Scripts/bootstrap.js"></script>
    <script src="/Scripts/modernizr-2.8.3.js"></script>

    <style>
        body{
            font-family: 'Work Sans', sans-serif;
            background-color: #F0D9FC;
        }

        input[type=text], select {
            width: 60%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .navbar-brand{
            font-size: 40px;
        }

        .navbar-nav{
            size: 300px;
        }

        .text-size{
            font-size: 22px;
            margin-top: 20px;
            }

        .btn-left, .btn-right{
            background-color: #2D2C7A;
            color: #DFB6FF;
            box-shadow: 0 0 0 1px #DFB6FF;
            border-radius: 50px;
            font-size: 15px;
        }

        .btn-left:hover, .btn-right:hover{
            background-color: #DFB6FF;
            color: #2D2C7A;
            box-shadow: 0 0 0 1px #1A1A47;
        }
    </style>

</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" style="background-color: #2D2C7A; border-color: #2D2C7A">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/" style="color: #DFB6FF; margin-top: 10%;margin-bottom: 10%;">Repair it!</a>
            </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a class="text-size" href="/" style="color: #DFB6FF">Strona główna</a></li>
                <li><a class="text-size" href="/Offer" style="color: #DFB6FF">Zlecenia</a></li>
                <li><a class="text-size" href="/Handyman" style="color: #DFB6FF">Usługi</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container body-content" style="justify-content: center;width: 1100px;margin-top: 50px">

    <h3 class="text-danger">Wystąpił błąd podczas przetwarzania żądania</h3>

    <br/>

    <a class="btn btn-lg btn-right" href="/">Powrót do strony głównej</a>

    <hr style="background-color: #2D2C7A; height: 1px;">
    <footer>
        <p style="color: #2D2C7A;">&copy; 2022 - Władowska Gabriela</p>
    </footer>
</div>
</body>
</html>