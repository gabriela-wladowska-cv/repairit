﻿namespace RepairIt.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Models;
    using Service = Models.Service;

    public class HandymanController : Controller
    {
        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            var model = new HandymanMapViewModel
                        {
                            ServiceFilter = ""
                        };

            using (var dbContext = ApplicationDbContext.Create())
            {
                model.Handymen = dbContext.Handymen.Select(x => new HandymanViewModel
                                                                {
                                                                    Id = x.Id,
                                                                    City = x.City,
                                                                    Street = x.Street,
                                                                    HomeNumber = x.HomeNumber,
                                                                    FlatNumber = x.FlatNumber,
                                                                    Radius = x.Radius,
                                                                    Description = x.Description,
                                                                    CreationDate = x.CreationDate,
                                                                    Services = x.Services.Select(y => y.ServiceName).ToList(),
                                                                    PhoneNumber = x.User.PhoneNumber
                                                                }).ToList();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(HandymanMapViewModel model)
        {
            List<HandymanViewModel> result;
            var service = model.ServiceFilter;

            using (var dbContext = ApplicationDbContext.Create())
            {
                if (service == Services.All)
                {
                    result = dbContext.Handymen.Select(x => new HandymanViewModel
                                                            {
                                                                Id = x.Id,
                                                                City = x.City,
                                                                Street = x.Street,
                                                                HomeNumber = x.HomeNumber,
                                                                FlatNumber = x.FlatNumber,
                                                                Radius = x.Radius,
                                                                Description = x.Description,
                                                                CreationDate = x.CreationDate,
                                                                Services = x.Services.Select(y => y.ServiceName).ToList(),
                                                                PhoneNumber = x.User.PhoneNumber
                                                            }).ToList();
                }
                else
                {
                    result = dbContext.Handymen.Where(x => x.Services.Select(y => y.ServiceName).FirstOrDefault() == service)
                                      .Select(x => new HandymanViewModel
                                                   {
                                                       Id = x.Id,
                                                       City = x.City,
                                                       Street = x.Street,
                                                       HomeNumber = x.HomeNumber,
                                                       FlatNumber = x.FlatNumber,
                                                       Radius = x.Radius,
                                                       Description = x.Description,
                                                       CreationDate = x.CreationDate,
                                                       Services = x.Services.Select(y => y.ServiceName).ToList(),
                                                       PhoneNumber = x.User.PhoneNumber
                                                   }).ToList();
                }
            }

            model.Handymen = result;
            model.ServiceFilter = "";

            if (!result.Any())
            {
                return RedirectToAction("Index", new {message = "Brak wyników - mapa nie została zmieniona"});
            }

            ViewBag.StatusMessage = "Wyświetlono przefiltrowane wyniki";

            return View(model);

        }

        [Authorize]
        public ActionResult AddHandyman()
        {
            var model = new HandymanViewModel();

            using (var dbContext = ApplicationDbContext.Create())
            {
                var id = User.Identity.GetUserId();
                var profile = dbContext.UsersProfile.FirstOrDefault(x => x.Id == id);

                model.PhoneNumber = profile?.PhoneNumber;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AddHandyman(HandymanViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var dbContext = ApplicationDbContext.Create())
            {
                var userId = User.Identity.GetUserId();
                var user = dbContext.UsersProfile.FirstOrDefault(x => x.Id == userId);
                var handyman = new Handyman
                               {
                                   City = model.City,
                                   Street = model.Street,
                                   HomeNumber = model.HomeNumber,
                                   FlatNumber = model.FlatNumber,
                                   CreationDate = DateTime.Now.Date,
                                   Radius = model.Radius,
                                   Description = model.Description,
                                   Services = new List<Service>(),
                                   User = user
                               };

                foreach (var service in model.Services)
                {
                    var existingService = dbContext.Services.FirstOrDefault(x => x.ServiceName == service);

                    if (existingService == null)
                    {
                        var dbService = new Service
                                        {
                                            ServiceName = service
                                        };

                        dbContext.Services.Add(dbService);
                        handyman.Services.Add(dbService);
                    }
                    else
                    {
                        handyman.Services.Add(existingService);
                    }
                }

                dbContext.Handymen.Add(handyman);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Usługa została pomyślnie dodana!"});
        }

        public ActionResult Details(Guid id)
        {
            HandymanViewModel model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var handyman = dbContext.Handymen.Select(x => new HandymanViewModel
                                                              {
                                                                  Id = x.Id,
                                                                  City = x.City,
                                                                  Street = x.Street,
                                                                  HomeNumber = x.HomeNumber,
                                                                  FlatNumber = x.FlatNumber,
                                                                  Radius = x.Radius,
                                                                  Description = x.Description,
                                                                  CreationDate = x.CreationDate,
                                                                  Services = x.Services.Select(y => y.ServiceName).ToList(),
                                                                  PhoneNumber = x.User.PhoneNumber
                                                              })
                                        .FirstOrDefault(x => x.Id == id);

                if (handyman == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Nie udało się uzyskać szczegółów usługi"});
                }

                model = handyman;
            }

            return View(model);
        }

        [Authorize]
        public ActionResult Edit(Guid id)
        {
            HandymanViewModel model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var handymanToEdit = dbContext.Handymen.FirstOrDefault(x => x.Id == id);

                if (handymanToEdit == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", "Manage", new {message = "Nie udało się przejść do edycji usługi"});
                }

                model = new HandymanViewModel
                        {
                            Id = handymanToEdit.Id,
                            City = handymanToEdit.City,
                            Street = handymanToEdit.Street,
                            HomeNumber = handymanToEdit.HomeNumber,
                            FlatNumber = handymanToEdit.FlatNumber,
                            Radius = handymanToEdit.Radius,
                            Description = handymanToEdit.Description,
                            CreationDate = handymanToEdit.CreationDate,
                            PhoneNumber = handymanToEdit.User.PhoneNumber,
                            Services = handymanToEdit.Services.Select(y => y.ServiceName).ToList()
                        };
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(HandymanViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var dbContext = ApplicationDbContext.Create())
            {
                var handymanToEdit = dbContext.Handymen.FirstOrDefault(x => x.Id == model.Id);

                if (handymanToEdit == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", "Manage", new {message = "Wystąpił błąd podczas edycji usługi"});
                }

                handymanToEdit.City = model.City;
                handymanToEdit.Description = model.Description;
                handymanToEdit.Radius = model.Radius;
                handymanToEdit.Street = model.Street;
                handymanToEdit.FlatNumber = model.FlatNumber;
                handymanToEdit.HomeNumber = model.HomeNumber;
                handymanToEdit.Services = new List<Service>();

                foreach (var service in model.Services)
                {
                    var existingService = dbContext.Services.FirstOrDefault(x => x.ServiceName == service);

                    if (existingService == null)
                    {
                        var dbService = new Service
                                        {
                                            ServiceName = service
                                        };

                        dbContext.Services.Add(dbService);
                        handymanToEdit.Services.Add(dbService);
                    }
                    else
                    {
                        handymanToEdit.Services.Add(existingService);
                    }
                }

                var userId = User.Identity.GetUserId();
                handymanToEdit.User = dbContext.UsersProfile.FirstOrDefault(x => x.Id == userId);

                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", "Manage", new {message = "Zedytowano usługę"});
        }

        [Authorize]
        public ActionResult Delete(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var handymanToDelete = dbContext.Handymen.FirstOrDefault(x => x.Id == id);

                if (handymanToDelete == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", "Manage", new {message = "Nie udało się usunąć usługi"});
                }

                dbContext.Handymen.Remove(handymanToDelete);
                dbContext.SaveChanges();
            }
            
            return RedirectToAction("Index", "Manage", new {message = "Usługa została usunięta"});
        }
    }
}
