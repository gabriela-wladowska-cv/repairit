﻿namespace RepairIt.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Models;

    public class OfferController : Controller
    {
        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            List<OfferViewModel> model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                model = dbContext.Offers.Select(x => new OfferViewModel
                                                     {
                                                         Id = x.Id,
                                                         Title = x.Title,
                                                         Description = x.Description,
                                                         City = x.City,
                                                         Street = x.Street,
                                                         HomeNumber = x.HomeNumber,
                                                         FlatNumber = x.FlatNumber,
                                                         CreationDate = x.CreationDate,
                                                         PhoneNumber = x.User.PhoneNumber
                                                     }).ToList();
            }
            return View(model);
        }

        [Authorize]
        public ActionResult AddOffer()
        {
            var model = new OfferViewModel();

            using (var dbContext = ApplicationDbContext.Create())
            {
                var id = User.Identity.GetUserId();
                var profile = dbContext.UsersProfile.FirstOrDefault(x => x.Id == id);

                model.PhoneNumber = profile?.PhoneNumber;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult AddOffer(OfferViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var dbContext = ApplicationDbContext.Create())
            {
                var id = User.Identity.GetUserId();
                var profile = dbContext.UsersProfile.FirstOrDefault(x => x.Id == id);
                var offer = new Offer
                            {
                                Title = model.Title,
                                Description = model.Description,
                                City = model.City,
                                Street = model.Street,
                                HomeNumber = model.HomeNumber,
                                FlatNumber = model.FlatNumber,
                                CreationDate = DateTime.Now.Date,
                                User = profile
                            };

                dbContext.Offers.Add(offer);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", new {message = "Zlecenie zostało pomyślnie dodane!"});
        }

        public ActionResult Details(Guid id)
        {
            OfferViewModel model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var offer = dbContext.Offers.Select(x => new OfferViewModel
                                                         {
                                                             Id = x.Id,
                                                             Title = x.Title,
                                                             Description = x.Description,
                                                             City = x.City,
                                                             Street = x.Street,
                                                             HomeNumber = x.HomeNumber,
                                                             FlatNumber = x.FlatNumber,
                                                             CreationDate = x.CreationDate,
                                                             PhoneNumber = x.User.PhoneNumber
                                                         })
                                        .FirstOrDefault(x => x.Id == id);

                if (offer == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", new {message = "Nie udało się uzyskać szczegółów zlecenia."});
                }

                model = offer;
            }

            return View(model);
        }

        [Authorize]
        public ActionResult Edit(Guid id)
        {
            OfferViewModel model;

            using (var dbContext = ApplicationDbContext.Create())
            {
                var offerToEdit = dbContext.Offers.FirstOrDefault(x => x.Id == id);

                if (offerToEdit == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", "Manage", new {message = "Nie udało się przejść do edycji zlecenia."});
                }

                model = new OfferViewModel
                        {
                            Id = offerToEdit.Id,
                            Title = offerToEdit.Title,
                            Description = offerToEdit.Description,
                            City = offerToEdit.City,
                            Street = offerToEdit.Street,
                            HomeNumber = offerToEdit.HomeNumber,
                            FlatNumber = offerToEdit.FlatNumber,
                            CreationDate = offerToEdit.CreationDate,
                            PhoneNumber = offerToEdit.User.PhoneNumber
                        };
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(OfferViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                using (var dbContext = ApplicationDbContext.Create())
                {
                    var offerToEdit = dbContext.Offers.FirstOrDefault(x => x.Id == model.Id);

                    if (offerToEdit == null)
                    {
                        dbContext.Dispose();
                        return RedirectToAction("Index", "Manage", new {message = "Wystąpił błąd podczas edycji zlecenia."});
                    }

                    offerToEdit.City = model.City;
                    offerToEdit.Description = model.Description;
                    offerToEdit.Street = model.Street;
                    offerToEdit.Title = model.Title;
                    offerToEdit.FlatNumber = model.FlatNumber;
                    offerToEdit.HomeNumber = model.HomeNumber;

                    var userId = User.Identity.GetUserId();
                    offerToEdit.User = dbContext.UsersProfile.FirstOrDefault(x => x.Id == userId);

                    dbContext.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                      eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                          ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            return RedirectToAction("Index", "Manage", new {message = "Zedytowano zlecenie"});
        }

        [Authorize]
        public ActionResult Delete(Guid id)
        {
            using (var dbContext = ApplicationDbContext.Create())
            {
                var offerToDelete = dbContext.Offers.FirstOrDefault(x => x.Id == id);

                if (offerToDelete == null)
                {
                    dbContext.Dispose();
                    return RedirectToAction("Index", "Manage", new {message = "Nie udało się usunąć zlecenia"});
                }

                dbContext.Offers.Remove(offerToDelete);
                dbContext.SaveChanges();
            }

            return RedirectToAction("Index", "Manage", new {message = "Zlecenie zostało usunięte"});
        }
    }
}
