﻿namespace RepairIt.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Models;

    [Authorize]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        public ActionResult Index(string message)
        {
            ViewBag.StatusMessage = message;

            ProfileViewModel model = new ProfileViewModel();

            using (var dbContext = ApplicationDbContext.Create())
            {
                var id = User.Identity.GetUserId();
                var profile = dbContext.UsersProfile.FirstOrDefault(x => x.Id == id);
                model.UserDetails = new UserDetailsViewModel
                            {
                                FirstName = profile?.FirstName,
                                Surname = profile?.Surname,
                                PhoneNumber = profile?.PhoneNumber,
                                Email = User.Identity.GetUserName()
                            };

                model.Handymen = dbContext.Handymen.Where(x => x.User.Id == id)
                                          .Select(x => new HandymanViewModel
                                                       {
                                                           Id = x.Id,
                                                           City = x.City,
                                                           Street = x.Street,
                                                           HomeNumber = x.HomeNumber,
                                                           FlatNumber = x.FlatNumber,
                                                           Radius = x.Radius,
                                                           Description = x.Description,
                                                           CreationDate = x.CreationDate,
                                                           Services = x.Services.Select(service => service.ServiceName).ToList(),
                                                           PhoneNumber = x.User.PhoneNumber
                                                       }).ToList();
                model.Offers = dbContext.Offers.Where(x => x.User.Id == id)
                                        .Select(x => new OfferViewModel
                                                            {
                                                                Id = x.Id,
                                                                Title = x.Title,
                                                                Description = x.Description,
                                                                City = x.City,
                                                                Street = x.Street,
                                                                HomeNumber = x.HomeNumber,
                                                                FlatNumber = x.FlatNumber,
                                                                CreationDate = x.CreationDate,
                                                                PhoneNumber = x.User.PhoneNumber
                                                            }).ToList();
                foreach (var offer in model.Offers)
                {
                    offer.CreationDate = offer.CreationDate.Date;
                }
            }
            return View(model);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }

                return RedirectToAction("Index", new {message = "Hasło zostało zmienione"});
            }

            AddErrors(result);
            return View(model);
        }

        public ActionResult EditProfile()
        {
            UserDetailsViewModel model;
            using (var dbContext = ApplicationDbContext.Create())
            {
                var id = User.Identity.GetUserId();
                var profile = dbContext.UsersProfile.FirstOrDefault(x => x.Id == id);
                model = new UserDetailsViewModel
                            {
                                FirstName = profile?.FirstName,
                                Surname = profile?.Surname,
                                PhoneNumber = profile?.PhoneNumber,
                                Email = User.Identity.GetUserName()
                            };
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProfile(UserDetailsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var id = User.Identity.GetUserId();
            var user = await UserManager.FindByIdAsync(id);
            user.Email = model.Email;
            user.UserName = model.Email;

            await UserManager.UpdateAsync(user);

            using (var dbContext = ApplicationDbContext.Create())
            {
                var profile = dbContext.UsersProfile.FirstOrDefault(x => x.Id == id);
                if (profile != null)
                {
                    profile.FirstName = model.FirstName;
                    profile.Surname = model.Surname;
                    profile.PhoneNumber = model.PhoneNumber;
                }

                await dbContext.SaveChangesAsync();
            }

            await SignInManager.SignInAsync(user, false, false);

            return RedirectToAction("Index", new {message = "Zedytowano profil"});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}
