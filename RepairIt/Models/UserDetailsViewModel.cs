﻿namespace RepairIt.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class UserDetailsViewModel
    {
        [Required]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "Imię musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Nazwisko musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Nazwisko")]
        public string Surname { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Numer telefonu")]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Adres email")]
        public string Email { get; set; }
    }
}
