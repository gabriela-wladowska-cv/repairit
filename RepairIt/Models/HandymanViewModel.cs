﻿namespace RepairIt.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class HandymanViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Nazwa miejscowości")]
        public string City { get; set; }

        [StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwa ulicy musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Nazwa ulicy")]
        public string Street { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 1, ErrorMessage = "Numer domu musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Numer budynku")]
        public string HomeNumber { get; set; }

        [Range(1, 999, ErrorMessage = "Numer lokalu musi mieć długość od {1} do {2} znaków.")]
        [Display(Name = "Numer lokalu")]
        public short? FlatNumber { get; set; }

        [Required]
        [Range(1,100, ErrorMessage = "Zasięg usług musi mieć wartość od {1} do {2} km.")]
        [Display(Name = "Zasięg usługi [km]")]
        public short Radius { get; set; }

        [StringLength(200, MinimumLength = 5, ErrorMessage = "Opis musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Opis usługi")]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data utworzenia usługi")]
        public DateTime CreationDate { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Typ usługi")]
        public List<string> Services { get; set; }

    }
}
