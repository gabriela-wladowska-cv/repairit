﻿namespace RepairIt.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Handyman
    {
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości musi mieć długość od {2} do {1} znaków.")]
        public string City { get; set; }

        [StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwa ulicy musi mieć długość od {2} do {1} znaków.")]
        public string Street { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 1, ErrorMessage = "Numer domu musi mieć długość od {2} do {1} znaków.")]
        public string HomeNumber { get; set; }

        [Range(1, 999, ErrorMessage = "Numer lokalu musi mieć długość od {1} do {2} znaków.")]
        public short? FlatNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }

        [Required]
        [Range(1,1000, ErrorMessage = "Zasięg usług musi mieć wartość od {1} do {2} km.")]
        public short Radius { get; set; }

        [StringLength(200, MinimumLength = 5, ErrorMessage = "Opis musi mieć długość od {2} do {1} znaków.")]
        public string Description { get; set; }

        [Required]
        public virtual List<Service> Services { get; set; }

        [Required]
        public virtual User User { get; set; }
    }
}
