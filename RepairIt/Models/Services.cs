﻿namespace RepairIt.Models
{
    using System.Collections.Generic;

    public class Services
    {
        public const string All = "Każdy";
        public const string Plumber = "Hydraulik";
        public const string Electrician = "Elektryk";
        public const string Gardener = "Ogrodnik";
        public const string ConstructionWorker = "Robotnik budowlany";
        public const string Mason = "Murarz";
        public const string Plasterer = "Tynkarz";
        public const string Upholsterer = "Tapicer";
        public const string Roofer = "Dekarz";
        public const string Tiler = "Glazurnik";
        public const string Paver = "Brukarz";

        public static IEnumerable<string> GetServices()
        {
            return new List<string>
                   {
                       All,
                       Plumber,
                       Electrician,
                       Gardener,
                       ConstructionWorker,
                       Mason,
                       Plasterer,
                       Upholsterer,
                       Roofer,
                       Tiler,
                       Paver
                   };
        }
    }
}
