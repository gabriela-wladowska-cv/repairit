﻿namespace RepairIt.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class User
    {
        [Required]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "Imię musi mieć długość od {2} do {1} znaków.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Nazwisko musi mieć długość od {2} do {1} znaków.")]
        public string Surname { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual List<Offer> Offers { get; set; }
    }
}
