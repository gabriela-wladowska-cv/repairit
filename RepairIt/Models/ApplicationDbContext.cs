﻿namespace RepairIt.Models
{
    using System.Data.Entity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
#if DEBUG
        public ApplicationDbContext() : base("RepairItDatabaseConnection", throwIfV1Schema: false)
        {
        }
#else
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
#endif

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<User> UsersProfile { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<Handyman> Handymen { get; set; }
        public DbSet<Service> Services { get; set; }
    }
}
