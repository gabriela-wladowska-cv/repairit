﻿namespace RepairIt.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ProfileViewModel
    {
        [Required]
        public UserDetailsViewModel UserDetails { get; set; }

        [Required]
        public List<HandymanViewModel> Handymen { get; set; }

        [Required]
        public List<OfferViewModel> Offers { get; set; }
    }
}
