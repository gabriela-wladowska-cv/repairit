﻿namespace RepairIt.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class HandymanMapViewModel
    {
        [Required]
        public List<HandymanViewModel> Handymen { get; set; }

        public string ServiceFilter { get; set; }
    }
}
