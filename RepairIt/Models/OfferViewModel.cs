﻿namespace RepairIt.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class OfferViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 5, ErrorMessage = "Tytuł musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Tytuł zlecenia")]
        public string Title { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 5, ErrorMessage = "Opis musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Opis zlecenia")]
        public string Description { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Nazwa miejscowości")]
        public string City { get; set; }

        [StringLength(40, MinimumLength = 2, ErrorMessage = "Nazwa ulicy musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Nazwa ulicy")]
        public string Street { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 1, ErrorMessage = "Numer domu musi mieć długość od {2} do {1} znaków.")]
        [Display(Name = "Numer budynku")]
        public string HomeNumber { get; set; }

        [Range(1, 999, ErrorMessage = "Numer lokalu musi mieć długość od {1} do {2} znaków.")]
        [Display(Name = "Numer mieszkania")]
        public short? FlatNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Data utworzenia zlecenia")]
        public DateTime CreationDate { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }
    }
}
