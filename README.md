# RepairIt Repository

This repository contains 2 projects related to my engineering thesis.

## RepairIt

RepairIt is an application that allows users to add offers and services.

### Usage

You don't have to install anything or run any commands! Application is published on Microsoft Azure.

### Link to application

[RepairIt](https://repairit.azurewebsites.net/)

## Algorithm Comparer

Algorithm Comparer is a tool based on BenchmarkDotNet.
It contains a set of tests that measures efficiency of some encryption algorithms.

### Usage

The simplest way is to open the project in any IDE e.g. JetBrains Rider and run the application in Release mode.
